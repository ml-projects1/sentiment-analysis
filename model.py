import tweepy 
import re
from tweepy import OAuthHandler 
from textblob import TextBlob 


class Twitter: 
  def __init__(self):
    self.access_token = "####"
    self.access_token_secret = "####"
    self.api_key = "####"
    self.api_key_secret = "####"
    try:  
      self.auth = OAuthHandler(self.api_key, self.api_key_secret) 
      self.auth.set_access_token(self.access_token, self.access_token_secret) 
      self.api = tweepy.API(self.auth) 
    except: 
      print("Error: Authentication Failed") 

  def get_tweets(self, query, count = 100):
    query = "(" + query + ")" + " lang:en -is:retweet"
    query = query.encode()
    tweets = self.api.search(q = query, count = count)
    return tweets

  def clean_text(self, text):
    return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t]) |(\w+:\/\/\S+)", " ", text).split()) 

  def get_tweet_sentiment(self, text):
    text = self.clean_text(text)
    sentiment = TextBlob(text)
    response = "neutral"
    if sentiment.sentiment.polarity > 0:
      response = "positive"
    elif sentiment.sentiment.polarity < 0:
      response = "negative"
    return response

  def get_sentiment(self, query):
    tweets = self.get_tweets(query, 500)
    parsed_tweets = []
    for tweet in tweets:
      data_frame = {}
      data_frame['tweet'] = tweet.text
      data_frame['sentiment'] = self.get_tweet_sentiment(tweet.text)
      parsed_tweets.append(data_frame)

    postive = [_['tweet'] for _ in parsed_tweets if _['sentiment'] == 'positive']
    negative = [_['tweet'] for _ in parsed_tweets if _['sentiment'] == 'negative']
    neutral = [_['tweet'] for _ in parsed_tweets if _['sentiment'] == 'neutral']
    response = {
      "total_tweets_analyzed": len(parsed_tweets),
      "postive": (len(postive) / len(parsed_tweets)) * 100,
      "negative": (len(negative) / len(parsed_tweets)) * 100,
      "neutral": (len(neutral) / len(parsed_tweets)) * 100
    }
    return response

