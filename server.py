import sys
sys.path.append(".")
from model import Twitter
from flask import Flask, jsonify, request

app = Flask(__name__)

@app.route('/get-sentiment', methods = ['GET'])

def getSentiment():
  query = request.args.get('query')
  return jsonify(twitter.get_sentiment(query)) 

if __name__ == "__main__":
  twitter = Twitter()
  app.run(port = 5000, debug=True)